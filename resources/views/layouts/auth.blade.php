<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('css/nifty.min.css') }}" rel="stylesheet">
    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/demo/nifty-demo.min.css') }}" rel="stylesheet">
    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('plugins/pace/pace.min.css') }}" rel="stylesheet">
    <!-- Scripts -->    
    <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>


</head>
<body>
<div id="container" class="cls-container">
        
        <!-- BACKGROUND IMAGE -->
        <!--===================================================-->
        <div id="bg-overlay"></div>
            @yield('content')        
        <!-- DEMO PURPOSE ONLY -->
        <!--===================================================-->
        <div class="demo-bg">
            <div id="demo-bg-list">
                <div class="demo-loading"><i class="psi-repeat-2"></i></div>
                <img class="demo-chg-bg bg-trans active" src="{{ asset('img/bg-img/thumbs/bg-trns.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-1.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-2.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-3.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-4.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-5.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-6.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('img/bg-img/thumbs/bg-img-7.jpg') }}" alt="Background Image">
            </div>
        </div>
        <!--===================================================-->
</div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


        
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('js/nifty.min.js') }}"></script>




    <!--=================================================-->
    
    <!--Background Image [ DEMONSTRATION ]-->
    <script src="{{ asset('js/demo/bg-images.js') }}"></script>
    
</body>
</html>
