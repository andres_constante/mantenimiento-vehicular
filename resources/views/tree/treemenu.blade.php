@if ($item['submenu'] == [])
    <li {!! ($item['select']=='SI' ? "data-jstree='{\"selected\":true}'" : "") !!}> <!--<li data-jstree='{"selected":true}'> -->
        {{ Form::hidden('idmenu', $item['id']) }}
        {{ $item['menu'] }}
    </li>
@else
    <li>
        {{ Form::hidden('idmenu', $item['id']) }}
        {{ $item['menu'] }}
        <ul>
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <li {!! ($submenu['select']=='SI' ? "data-jstree='{\"selected\":true}'" : "" )!!}>
                        {{ Form::hidden('idmenu', $submenu['id']) }}
                        {{ $submenu['menu'] }}
                    </li>
                @else
                    @include('tree.treemenu', [ 'item' => $submenu ])
                @endif
            @endforeach
        </ul>
    </li>
@endif
