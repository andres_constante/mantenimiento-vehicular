<a href="{{ $ruta . '/create?id=' . $idsi }}" class="btn btn-primary ">Asignar Permisos</a>
<div id="jstree">
    <ul>
        <li class="jstree-open">Todos los Permisos
            <ul>
                @foreach ($menu as $key => $item)
                    @if ($item['parent'] != 0)
                    @break
                @endif
                @include('tree.treemenu', ['item' => $item,'sub'=>'NO','four'=>'NO' ])
                @endforeach
            </ul>
        </li>
    </ul>
</div>
@if ($submit == 1)
    @if ($idsi != 0)        
        {!! Form::submit('Grabar Permisos', ['class' => 'btn btn-primary', 'style' => 'float:right']) !!}
        
        {!! Form::button('Permisos por página', ['class' => 'btn btn-success', 'style' => 'float:right',"onclick"=>'llamarpermisoscrud()']) !!}
    @endif
@endif
