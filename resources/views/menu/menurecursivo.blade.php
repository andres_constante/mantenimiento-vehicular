@if ($item['submenu'] == [])
    <li>
        <a href="{{ ($item['ruta']=="" ? "#":url($item['ruta'])) }}">
            @if($item['icono']!="")
                <i class="{{ $item['icono'] }}"></i>
            @endif
            <span class="menu-title">{{ $item['menu'] }}</span>
        </a>
    </li>
@else
    <?php
        $submenustr="";
        $submenustr ='
        <li class="@classactive">
            <a href="#">';
        if($item['icono']!="")        
                    $submenustr .='<i class="'.$item['icono'].'"></i>';          
        $submenustr .='<span class="menu-title">'.$item['menu'] .'</span>
                <i class="arrow"></i>
            </a>
        ';
        $submenustr .='<ul class="collapse @classin">';
        foreach ($item['submenu'] as $submenu)
        {
            if ($submenu['submenu'] == [])
            {                    
                if(str_contains($submenu['ruta'],"http"))
                {
                    $submenustr .='<li>
                                    <a href="'.$submenu['ruta'].'" target="_blank">';
                    if($submenu['icono']!="")
                        $submenustr .='<i class="'.$submenu['icono'].'"></i>';
                    $submenustr .=$submenu['menu'] .'</a>
                                </li>';
                }
                elseif($submenu['ruta']=="" || $submenu['ruta']=="#")
                {
                    $submenustr .='<li><a href="#">';
                    if($submenu['icono']!="")
                        $submenustr .='<i class="'.$submenu['icono'].'"></i>';
                    $submenustr.=$submenu['menu'].'</a></li>';
                }
                else{
                    $submenustr .='<li '. (Request::is('*'.$submenu['ruta'].'*') ? 'class=active-link' : '').'>
                                    <a href="'.url($submenu['ruta']).'">';
                    if($submenu['icono']!="")
                        $submenustr .='<i class="'.$submenu['icono'].'"></i>';
                    $submenustr.=$submenu['menu'];
                    $submenustr .= '</a>
                        </li>';
                    if(Request::is('*'.$submenu['ruta'].'*'))
                    {
                        $submenustr=str_replace('@classactive','active-sub',$submenustr);
                        $submenustr=str_replace('@classin','in',$submenustr);
                    }
                }                
            }else{                 
                                       
                //@include('menu.menurecursivo', [ 'item' => $submenu,'sub'=>'SI','four'=>'SI']);       
                $submenustr.=   view('menu.menurecursivo', [ 'item' => $submenu,'sub'=>'SI','four'=>'SI']);
            }
        }    //endforeach
        $submenustr.="</ul>";
        echo $submenustr;
        //$submenustr="";
    ?> 
@endif