<?php
//if(!isset($nobloqueo))
    //Autorizar(Request::path());
$menu = getmenu();
$menuadmin = getmenu('SI');
?>
@extends('layouts.main')
@section ('titulo')  {{$configuraciongeneral[0]}} @stop
@section('estilos')
<style>
  .table-success, .table-success>td, .table-success>th {
    background-color: #c3e6cb;
  }
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
</style>
@stop
@section ('scripts')
<!-- Tree -->
{!! HTML::style('plugins/jstree/themes/default/style.min.css'); !!}
{!! Html::script('plugins/jstree/jstree.min.js') !!}
 <script type="text/javascript">
$(document).ready(function() {
 function cargapermisosp(sel,tipobus)
 {
  var div = "#divresul";
  var url='{!! URL::to($configuraciongeneral[3]) !!}';
  var ruta='{!! URL::to($configuraciongeneral[1]) !!}';
  //var sel = $(this);
  //alert(sel.val());
  //alert(url);
  $(div).html("Espere...");
  $.ajax({
      type: "GET",
      url: url,
      data: { id: sel.val(),menutodo: @if(isset($menutodo)) 1 @else 0 @endif, tipobus: tipobus, ruta: ruta },
      error: function(objeto, quepaso, otroobj){
        //alert(quepaso);
        $(div).html(quepaso);
      },        
      success: function(datos){
          //alert(datos);
        $(div).html(datos);
        @if(!isset($checkmenu))
              $('#jstree').jstree({
                'state': {
                                        'opened': true
                                        },
                                    'core' : {
                                        'check_callback' : true
                                    }                                    
                                }) ;
        @else
          $('#jstree').on('changed.jstree', function (e, data) {
                                    var i, j, r = [];
                                    for(i = 0, j = data.selected.length; i < j; i++) {
                                        r.push(data.instance.get_node(data.selected[i]).text);
                                        //r.push(data.instance.get_node(data.selected[i]).text.)
                                    }
                                    //$('#event_result').html('Selected: ' + r.join(', '));
                                    var valor = r.join(', ');
                                    valor= valor.replace(/idmenu/g,'idmenumain[]');
                                    //valor= valor.replace(/hidden/g,'text');
                                    $('#divselect').html(valor);
                                    //            alert (valor);
                                }).jstree({
                                    'state': {
                                        'opened': true
                                        },
                                    'core' : {
                                        'check_callback' : true
                                    },
                                    "checkbox" : {
                                        "keep_selected_style" : false
                                    },
                                    "plugins" : [ "checkbox" ]                                      
                                }) ;
        @endif

      },
        statusCode: {
          404: function() {
              //alert('No Existe URL');
          $(div).html(":(");
        } 
      }   
  });
 }
$("#name").change(function()
{
    return cargapermisosp($(this),"usuario");
});//select Change
$("#perfil").change(function()
{
    return cargapermisosp($(this),"perfil");
});//select Change
  

});
function llamarpermisoscrud()
{
  //$("#divresul").html("Hola");
  var sel = $("#name").val();
  if(sel === undefined)
    sel = $("#perfil").val();
  //alert(sel);
  var div = "#divresul";
  var url='{!! URL::to($configuraciongeneral[4]) !!}';
  var ruta='{!! URL::to($configuraciongeneral[1]) !!}';
  var tipobus = "{{ ($configuraciongeneral[1]=='asignarperfilpermisos' ? 'perfil' : 'usuario') }}";
  //var sel = $(this);
  //alert(sel.val());
  //alert(url);
  $(div).html("Espere...");
  $.ajax({
      type: "GET",
      url: url,
      data: { id: sel,menutodo: @if(isset($menutodo)) 1 @else 0 @endif, tipobus: tipobus, ruta: ruta },
      error: function(objeto, quepaso, otroobj){
        //alert(quepaso);
        $(div).html(quepaso);
      },        
      success: function(datos){
          //alert(datos);
        $(div).html(datos);
      },
        statusCode: {
          404: function() {
              //alert('No Existe URL');
          $(div).html(":(");
        } 
      }   
  });
}
</script>
@stop
@section ('content')
<div class="panel panel-body">
        <div class="panel-heading">
            <h3>{{ $configuraciongeneral[0] }}</h3>
            <p>
                <a href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary "><i
                        class="fa fa-list"></i>Todos</a>                
            </p>
        </div>
        <div class="panel-body">
          <p>

    @if($configuraciongeneral[2]=="crear")
     {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal', 'files' => true))  !!}
    @else
      {!!  Form::model($tabla, array('route' => array($configuraciongeneral[1].'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal', 'id'=>'form1' ))  !!}
    @endif
    
       @if(isset($objetos)) 
         @foreach($objetos as $i => $campos)               
                  @if($campos->Tipo=="htmltabla") 
                    <?php continue; ?>
                  @endif

                  @if($campos->Tipo=="datetext")                    
                      <div class="form-group" id="divobj-{{ $campos->Nombre }}">
                           {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                        
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                       </div>   
                     </div>   

                  @elseif($campos->Tipo=="date")

                   <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 

                      {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      
                     </div>   
                   </div>  

                   @elseif($campos->Tipo=="timefecha")

                     <div class="form-group">
                      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 
                        <div class="input-group bootstrap-timepicker timepicker">
                         {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control timefecha input-small', $campos->Nombre]) !!}
                         <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div> 
                      </div>
                    </div>  

                   @elseif($campos->Tipo=="select")

                     <div class="form-group">
                      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 
                       {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : isset($listaanterior[$i]) ? $listaanterior[$i]:Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      </div>
                    </div>    

                   @elseif($campos->Tipo=="select2")

                    <div class="form-group">
                       {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8"> 
                       {!! Form::select($campos->Nombre,[null=>"Escoja..."]+(array)$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      </div> 
                    </div>   

                   @elseif($campos->Tipo=="select-multiple")

                    <div class="form-group">
                       {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                    </div>    

                     <div class="col-lg-8"> 
                       {!! Form::select($campos,$lista[$i], Input::old($campos) ? Input::old($campos) : isset($listaanterior[$i]) ? $listaanterior[$i]:Input::old($campos), array('class' => "form-control ".$clase[$i])) !!}
                     </div>

                   @elseif($campos->Tipo=="file") 

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::file($campos->Nombre, array('class' => 'form-control')) !!}
                       </div>  
                      </div>   

                   @elseif($campos->Tipo=="textdisabled")

                   <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::text($campos->Nombre, $campos->Valor , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion ,'readonly')) !!}
                      </div>  
                    </div>    

                   @elseif($campos->Tipo=="textarea")

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}

                         <div class="col-lg-8"> 
                            {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                        </div>  
                      </div>     

                   @elseif($campos->Tipo=="password")

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}

                       <div class="col-lg-8"> 
                         {!!   Form::password($campos,array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i])) !!}
                       </div> 

                      </div>  

                   @elseif($campos->Tipo=="Boton")

                      <div class="form-group">
                       <div class="col-lg-11"> 
                        {!!  Form::submit($campos->Descripcion, array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
                       </div>  
                      </div> 

                   @elseif($campos->Tipo=="FormHiddenJson")

                     <div class="form-group">
                       <div class="col-lg-12"> 
                        {!! Form::hidden($campos->Nombre,json_encode($campos->Valor) ) !!}
                       </div> 
                     </div>  

                   @elseif($campos->Tipo=="FormHidden")
                        {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}

                   @elseif($campos->Tipo=="filehtml")

                     <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                        <a href="{{URL::to('local/public'.$campos->Valor)}}" target="_blank">  Ver Archivo </a>
                        {!! Form::hidden($campos->Nombre,$campos->Valor ) !!}  
                       </div>  
                      </div>    

                   @elseif($campos->Tipo=="html")

                    <div class="form-group" id="divobj-{{ $campos->Nombre }}">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8" id="div-{{ $campos->Nombre }}"> 
                        <?php echo $campos->Valor; ?>  
                      </div>  
                    </div>  

                   @elseif($campos->Tipo=="html2")

                    <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8"> 
                        <?php echo $campos->Valor; ?>  
                      </div>  
                    </div>  

                   @elseif($campos->Tipo=="htmlplantilla")
                    
                    <div class="form-group">
                      <div class="col-lg-12"> 
                        <?php echo $campos->Valor; ?>  
                      </div> 
                    </div>


                @elseif($campos->Tipo=="link")
                
                   <div class="form-group">
                     {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                   <div class="col-lg-8"> 
                    <a href="{{URL::to($campos->Valor)}}" target="_blank" >  {{$campos->Descripcion}} </a>
                   </div>  
                  </div>    

                 @elseif($campos->Tipo=="botonjs")
                  <div class="form-group">
                   <div class="col-lg-11"> 
                  <a id="{{$campos->Nombre}}" class=" btn btn-primary pull-right "> {{$campos->Descripcion}} </a>
                    <a> </a>
                   </div>  
                  </div> 

                  @elseif($campos->Tipo=="funcionjs")
                  
                     <?php echo $campos->Valor; ?>  


                  @elseif($campos->Tipo=="htmljs")
                   <div class="col-lg-11"> 
                     <?php echo $campos->Valor; ?>     
                    </div>  
                  @elseif($campos->Tipo=="checkbox")

                     <div class="form-group">
                         
                       <div class="col-lg-11"> 
                           {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-9 control-label")) !!}
                           {!! Form::checkbox($campos->Nombre,null,null, array('id'=>$campos->Nombre)) !!}    
                      </div>  
                    </div>  
                @elseif($campos->Tipo=="divresul")
                     <div class="form-group">
                         
                       <div class="col-lg-11" id="{{ $campos->Nombre }}"> 
                           
                      </div>  
                    </div>  
                  @elseif($campos->Tipo=="botontabla" || $campos->Tipo=="botontablapopup")


                  @else
                    <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      </div>  
                    </div>    
                  @endif
                      
        @endforeach
      @endif  
      <div id="divselect" style="display:none">
    
      </div>
<div id="divresul"></div>

        {!!  Form::close()  !!}
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>    
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
        <div class="alert alert-danger" style="text-align: left; width:75%;display: table; margin: 0 auto;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Aviso!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div> 
        </div>
    @endif  
    @if (Session::has('message'))    
        <div class="alert alert-info" style="text-align: left; width:75%;display: table; margin: 0 auto;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Mensaje !</h4>
            <ul><li>{!!  Session::get('message')  !!}</li></ul>
        </div>
    @endif    
        
          </p>
</div> <!--  panel-body-->


<script type="text/javascript">

//$(".popupboton").colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.4), innerHeight:screen.height -(screen.height * 0.3)}); 
$(document).ready(function() {
  //alert("holaa");
  setTimeout(function(){
    $("#name").trigger("change");
  $("#perfil").trigger("change");
  },500);
  
});
</script>    
</div>
@stop

