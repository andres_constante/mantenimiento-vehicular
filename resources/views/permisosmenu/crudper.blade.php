<script type="text/javascript">
    function actualizarpercrud(id,tipobus) {
        var crear = $("#selcrear" + id).val();
        var editar = $("#seleditar" + id).val();
        var eliminar = $("#seleliminar" + id).val();
        var div = "#divresul";
        //console.log(crear + editar + eliminar);
        $.ajax({
            type: "POST",
            url: "{{ URL::to("llamarpermisoscrudajax")}}",
            data: {
                id: '{{$idusuario}}',
                crear: crear,
                editar: editar,
                eliminar: eliminar,
                tipobus: tipobus,
                idpermiso: id,
                _token:'{{ csrf_token() }}'
            },
            error: function(objeto, quepaso, otroobj) {
                //alert(quepaso);
                $(div).html(quepaso);
            },
            success: function(datos) {
                //alert(datos);
                $(div).html(datos);
                $("#tr-"+id).addClass("table-success");
            },
            statusCode: {
                404: function() {
                    //alert('No Existe URL');
                    $(div).html(":(");
                }
            }
        });
    }

</script>
<table id="mainTable" class="table table-striped table-bordered table-hover display">
    <thead>
        <tr>
            <th>ID</th>
            <th>Enlace</th>
            <th>Crear</th>
            <th>Editar</th>
            <th>Eliminar</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tabla as $key => $value)
            <tr id="tr-{{ $value->id }}">
                <td>{{ $value->id }}</td>
                <td>{{ $value->menu }}</td>
                <td>{{ Form::select('Escoja', $sino, $value->crear, ['id' => 'selcrear' . $value->id]) }}</td>
                <td>{{ Form::select('Escoja', $sino, $value->editar, ['id' => 'seleditar' . $value->id]) }}</td>
                <td>{{ Form::select('Escoja', $sino, $value->eliminar, ['id' => 'seleliminar' . $value->id]) }}</td>
                <td>
                    <a class="botonactualizar" id="nuevoguardar" href="javascript:"
                        onclick="actualizarpercrud({{ $value->id }},'{{ $tipobus }}')">
                        <i class="fa fa-save fa-2x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
