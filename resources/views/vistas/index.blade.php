<?php
//if(!isset($nobloqueo))
// Autorizar(Request::path());
$menu = getmenu();
$menuadmin = getmenu('SI');
?>
@extends('layouts.main')
@section('titulo') {{ $configuraciongeneral[0] }} @stop
@section('scripts')
    <!-- DataTable -->
    {{ HTML::script('plugins/datatables/media/js/jquery.dataTables.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/dataTables.buttons.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/buttons.flash.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/jszip.min.js') }}
    {{-- HTML::script('plugins/datatables/media/js/pdfmake.min.js') --}}
    {{ HTML::script('plugins/datatables/media/js/vfs_fonts.js') }}
    {{ HTML::script('plugins/datatables/media/js/buttons.html5.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/buttons.print.min.js') }}
    <!-- Data Tables -->
    {{ HTML::script('plugins/datatables/media/js/dataTables.bootstrap.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/dataTables.responsive.min.js') }}
    {{ HTML::script('plugins/datatables/media/js/dataTables.tableTools.min.js') }}

    <!-- Data Tables css-->
    {{ HTML::style('plugins/datatables/media/css/jquery.dataTables.min.css') }}
    {{ HTML::style('plugins/datatables/media/css/dataTables.bootstrap.min.css') }}
    {{ HTML::style('plugins/datatables/media/css/dataTables.responsive.css') }}
    {{ HTML::style('plugins/datatables/media/css/dataTables.tableTools.min.css') }}
    {{ HTML::style('plugins/datatables/media/css/buttons.dataTables.min.css') }}

    <!-- ColorBox-->
    {{ HTML::style('plugins/colorbox/colorbox.css') }}
    {{ HTML::script('plugins/colorbox/jquery.colorbox.js') }}
    <script type="text/javascript">
        var tabla = null;
        $(document).ready(function() {

            $(".mayuscula").on('keyup', function(e) {
                $(this).val($(this).val().toUpperCase());
            });

            $('.solonumeros').on('input', function() {
                this.value = this.value.replace(/[^0-9.]/g, '');
            });

            // $(".loader_panel").fadeIn("slow");
            // $("#modal_denuncia").modal("show");

            $('#c_observaciones').hide();

            $("#estado").change(function(e) {
                if ($("#estado").val() == 'FINALIZADO') {

                    $('#observacion_respuesta').show();
                    $('#c_observaciones').hide();
                } else {
                    $('#c_observaciones').show();
                    $('#observacion_respuesta').hide();
                    getRespuestas();
                }
            });


            var URL_ = "{{ URL::to("$configuraciongeneral[6]") }}";
            tabla = $('#tbbuzonmain').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: URL_,
                    dataType: "json",
                    type: "GET",
                    data: {
                        _token: "{{ csrf_token() }}"
                    }
                },
                columns: [{
                        "data": 'id'
                    },
                    @foreach ($objetos as $key => $value)
                        {
                        "data": '{{ $value->Nombre }}'
                        },
                    @endforeach

                    {
                        "data": 'acciones'
                    },
                ],
                // dom: 'T<"clear">lfrtip',
                // tableTools: {
                //     sSwfPath: "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                // },
                lengthMenu: [
                    [20, 30, 50, 100, -1],
                    [20, 30, 50, 100, "All"]
                ],
                order: ([0, 'desc']),
                dom: 'flrBtip',
                //buttons: ['print', 'copy', 'excel', 'pdf'],
                buttons: [
                    'csv',
                    'excel',
                    //'pdf',
                    'print'
                ],
                language: {
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontraron registros coincidentes",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Atrás"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                // responsive: true,

            });

            // $('#container').css( 'display', 'block' );
            // tabla.columns.adjust().draw();
        });
        @if ($permisos->eliminar == 'SI')
            function eliminarswal($id) {
            Swal.fire({
            title: "Seguro de eliminar este registro?",
            text: "Una vez eliminado este registro no podrá ser recuperado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar",
            cancelButtonText: "Cancelar",
            }).then((result) => {
            if (result.value) {
            $('#frmElimina' + $id).submit();
            }
            });
            }
        
        @endif


        function popup(obj) {
            $(obj).colorbox({
                fixed: true,
                iframe: true,
                innerWidth: screen.width - (screen.width * 0.20),
                innerHeight: screen.height - (screen.height * 0.40)
            });
        }
        @if (isset($permisos))
            @if ($permisos->eliminar == 'SI')
                function eliminar($id)
                {
                var r= confirm("Seguro de eliminar este registro?");
                if(r==true)
                $('#frmElimina'+$id).submit();
                else
                return false;
                }
            @endif
        @endif
        setTimeout(function(){
        parent.$.fn.colorbox.close();
            },1000);
    </script>

@stop

@section('estilos')

    <style>
        body.DTTT_Print {
            background: #fff;

        }

        .DTTT_Print #page-wrapper {
            margin: 0;
            background: #fff;
        }

        button.DTTT_button,
        div.DTTT_button,
        a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        button.DTTT_button:hover,
        div.DTTT_button:hover,
        a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        .dataTables_filter label {
            margin-right: 5px;

        }

    </style>

@stop
@section('content')
    <div class="panel panel-body">
        <div class="panel-heading">
            <h3>{{ $configuraciongeneral[0] }}</h3>
            <p>
                <a href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary "><i
                        class="fa fa-list"></i>Todos</a>
                @if ($permisos->crear == 'SI')
                    <a href="{{ URL::to($configuraciongeneral[1]) . '/create' }}" class="btn btn-default ">
                        <i class="fa fa-plus"></i> Nuevo</a>
                @endif
            </p>
        </div>
        <div class="panel-body">
            <!-- Mensajes y Alertas -->
            @if (Session::has('message'))
                <p>
                    <br>
                    <script>
                        $(document).ready(function() {
                            //toastr.succes("{{ Session::get('message') }}");
                            toastr["success"]("{{ Session::get('message') }}");
                            //$.notify("{{ Session::get('message') }}","success");
                        });

                    </script>
                <div class="alert alert-info alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×
                    </button>{{ Session::get('message') }}
                </div>
            @elseif (Session::has('error'))
                <script>
                    $(document).ready(function() {
                        //toastr.succes("{{ Session::get('error') }}");
                        toastr["error"]("{{ Session::get('error') }}");
                        //$.notify("{{ Session::get('error') }}","success");
                    });

                </script>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×
                    </button>{{ Session::get('error') }}
                </div>
                </p>
            @endif
            <!-- INICIO FORMULARIO -->
            <p>
                <br>
            <div class="table-responsive">
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>
                            @if (!isset($verid))
                                <th>ID</th>
                            @endif
                            @foreach ($objetos as $key => $value)
                                <th>{{ $value->Descripcion }}</th>
                            @endforeach
                            <th>Acción</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            @if (!isset($verid))
                                <th>ID</th>
                            @endif
                            @foreach ($objetos as $key => $value)
                                <th>{{ $value->Descripcion }}</th>
                            @endforeach
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                    </tfoot>

                </table>
            </div><!-- table-responsive-->
            </p>
        </div>
    </div>
@stop
