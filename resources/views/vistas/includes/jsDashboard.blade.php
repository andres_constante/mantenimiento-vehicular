<script type="text/javascript">
    $(document).ready(function() {
        llenaestados(0);

        $("#direccion").change(function(e) {
            llenaestados(0);
        });
        $("#fecha_fin").change(function(e) {
            llenaestados(0);
        });
        $("#fecha_inicio").change(function(e) {
            llenaestados(0);
        });
        $("#estado").change(function(e) {
            llenaestados(0);
        });
        $("#fase").change(function(e) {
            llenaestados(0);
        });

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false, //True para mostrar Numero de semanas
            autoclose: true,
            format: 'yyyy-mm-dd',
            language: 'es'
        });


    });


    function limpiar() {
        @isset($estados)
        @foreach($estados as $k => $filtro)
        $("#{{$k}}").html('0');
        @endforeach
        @endisset
    }



    function llenaestados(id) {
        // alert(id);
        limpiar();
        var datos = new FormData();
        datos.append("_token", '{{ csrf_token() }}');
        @isset($filtros)
        @foreach($filtros as $k => $filtro)
        datos.append("{{$filtro->Nombre}}", $("#{{$filtro->Nombre}}").val());
        @endforeach
        @endisset
        var lista = [];
        var lista_final = [];
        var categorias = [];




        //gradico 2
        var lista_pie = [];
        var lista_final_pie = [];


        //grafico 3
        var lista_direciones = [];
        var lista_final_direciones = [];
        var categorias_direciones = [];

        //grafico 3
        var lista_direciones_estados = [];
        var lista_final_direciones_estados = [];
        var categorias_direciones_estados = [];
        var total = 0;

        $.ajax({
            type: "POST",
            url: "{{$configuraciongeneral[3]}}",
            data: datos,
            contentType: false,
            processData: false,
            cache: false,
            success: function(response) {
                console.log(response);
                if (response.estados) {
                    $.each(response.estados, function(i, item) {
                        try {

                            document.getElementById(i).innerHTML = item;

                            total = total + item;

                            document.getElementById('total_poa').innerHTML = total;
                            var color = "";

                            if (i == "POR_EJECUTAR") {
                                color = '#D0E0E3';
                            } else if (i == "EJECUCION_VENCIDO") {
                                color = '#F1C232';
                            } else if (i == "EJECUTADO") {
                                color = '#00FF00';
                            } else if (i == "EJECUCION") {
                                color = '#00FFFF';
                            } else if (i == "SUSPENDIDO") {
                                color = '#E06666';
                            } else if (i == "ANTEPROYECTO") {
                                color = '#00FF00';
                            } else if (i == "ESTUDIO") {
                                color = '#E06666';
                            } else if (i == "ESTUDIO_DEFINITIVO") {
                                color = '#00FFFF';
                            } else if (i == "ESTUDIO_POR_ACTUALIZAR") {
                                color = '#E06666';
                            } else if (i == "NINGUNO") {
                                color = '#E06666';
                            } else if (i == "PROYECTO") {
                                color = '#D0E0E3';
                            } else if (i == "TERMINOS_DE_REFERENCIA") {
                                color = '#F1C232';
                            } else {

                            }
                            lista.push({
                                y: item,
                                color: color
                            });
                            categorias.push(i);
                            lista_pie.push({
                                name: i,
                                color: color,
                                y: item
                            })

                        } catch (error) {
                            console.log(error);

                        }
                    })


                    lista_final.push({
                        "name": 'ESTADOS',
                        "data": lista
                    })
                    lista_final_pie.push({
                        "name": 'ESTADOS',
                        "data": lista_pie
                    })
                    armar_grafico('linechart_material_barras', 'bar', response.titulo, response.detalle + ' agrupadas por estados ', null, 'ACTIVIDADES POR ESTADO', lista_final, categorias,response.tipo,'estado',500);
                    //armar_grafico('linechart_material_two', 'pie', response.titulo, response.detalle + ' agrupadas por estados ', ['#00FFFF', '#F1C232', '#00FF00', '#E06666', '#D0E0E3', '#E06666'], 'ACTIVIDADES POR ESTADO', lista_final_pie, [],response.tipo,'estado',1000);
                }


                var color1=0;
                if($("#estado").val()=="EJECUCION_VENCIDO"){
                    color1=1;
                }
                if (response.direciones_poa) {
                    
                    $.each(response.direciones_poa, function(i, item) {
                        try {
                            // color1item-1;

                            if(color1==1){
                                // console.log(rgbToHex(255,((Math.random() * (150 - 20) + 20)), 0));
                                var color =Math.round((Math.random() * (150 - 20) + 20));
                                lista_direciones.push({
                                    y: item,
                                    color:rgbToHex(255,color, 0)

                                });
                            }else{
                                lista_direciones.push({
                                    y: item,
                                    color:getRandomColor()
                                });
                            }
                            
                            categorias_direciones.push(i);


                        } catch (error) {
                            console.log(error);

                        }
                    })


                    lista_final_direciones.push({
                        "name": 'Actividades',
                        "data": lista_direciones
                    })

                    armar_grafico('linechart_material_barras_entidades', 'bar', response.titulo, response.detalle + '  agrupadas por direcciones', ['#333343'], 'ACTIVIDADES POR DIRECCIONES', lista_final_direciones, categorias_direciones,response.tipo,'direccion',1000);
                }
                // if (response.direciones_por_estados) {
                //     var colores=[];
                //     $.each(response.direciones_por_estados, function(i, item) {
                //         try {


                //             var datos=[];
                //             $.each(item, function(i_, item_) {
                //                 datos.push(item_);

                //             })
                //             lista_final_direciones_estados.push({
                //                 "name": i,
                //                 "data": datos,
                //                 "color":"#BF0B23"
                //             })
                //             categorias_direciones_estados.push(i)


                //         } catch (error) {
                //             console.log(error);

                //         }
                //     })


                //   console.log(lista_final_direciones_estados);

                //     categorias.push('SIN_ESTADO');
                //     armar_grafico('linechart_material_direcciones_estados', 'bar', response.titulo, response.detalle+'  agrupadas por direcciones',null, 'ACTIVIDADES POR DIRECCIONES', lista_final_direciones_estados, categorias);
                // }


            }
        });



    }

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    function getRandomColorRojo() {
        var color = '#';
        
        color+="ff00"+ Math.floor(Math.random() * 3)+10;
        console.log(color);
        return color;
    }

    function armar_grafico(id_div, tipo, titulo, sub_titulo, colores, nombre_grafico, datos, categorias,tipo_dashboard,tipo_grafico,tamanio) {
        Highcharts.chart(id_div, {
            chart: {
                type: tipo,
                marginLeft: 180,
                height:tamanio,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
            },
            title: {
                text: titulo
            },
            subtitle: {
                text: '<h1>' + sub_titulo + '</h2>',
                fontSize: '20px'
            },
            xAxis: {
                categories: categorias,
                title: {
                    text: null
                },
                min: 0,

                scrollbar: {
                    enabled: true
                },
                tickLength: 0
            },
            colors: colores,
            yAxis: {
                min: 0,
                title: {
                    text: '<b>',
                    align: 'low'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    events: {
                        click: function(event) {
                            console.log(event);
                            mostrarAlert(event,tipo_dashboard,tipo_grafico);
                        }
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: datos
        });
    }


    function mostrarAlert(categoria,tipo,tipo_grafico) {
        var datos = new FormData();
        datos.append("_token", '{{ csrf_token() }}');
        datos.append("tipo", tipo);
        @isset($filtros)
        @foreach($filtros as $k => $filtro)
        datos.append("{{$filtro->Nombre}}", $("#{{$filtro->Nombre}}").val());
        @endforeach
        @endisset

        var punto = '';
        if (event.point.category != undefined) {

            punto = event.point.category;
        } else {
            punto = event.point.name;
        }
        datos.append("categoria", punto);
        datos.append("tipo_grafico", tipo_grafico);
        $.ajax({
            type: "POST",
            url: "{{ URL::to('') }}/coordinacioncronograma/detalledasboard",
            data: datos,
            contentType: false,
            processData: false,
            cache: false,
            success: function(response) {
                
                Swal.fire({
                    title: response.msg,
                    text: "DETALLE",
                    html:'<div style="overflow: scroll;width: 100%; height: 500px;">'+ response+'</div>',
                    showCancelButton: true,
                    confirmButtonText: 'Imprimir',
                    cancelButtonText: 'Ok',
                    width: '80%',
                    showLoaderOnConfirm: true,
                    preConfirm: (codigo) => {
                            // alert('ss');
                            console.log(codigo);
                        if (codigo) {
                            var printWindow = window.open("", "Imprimir" );
                            $("link, style").each(function() {
                                $(printWindow.document.head).append('<div style="width: 100%; height: 500px;">'+ response+'</div>')
                            });
                            var toInsert = $('<div style="width: 100%; height: 500px;">'+ response+'</div>').clone();
                            console.log(toInsert);
                            var divheader= $("#divheader").html();
                            toInsert= toInsert.html(divheader+toInsert.html());
                            $(printWindow.document.body).append(toInsert);
                            setTimeout(function(){
                                printWindow.print();
                            },1000);
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });

            }
        });

    }
</script>