<?php
//if(!isset($nobloqueo))
// Autorizar(Request::path());
$menu = getmenu();
$menuadmin = getmenu('SI');
?>
<!-- extends ('layouts.main') -->
@extends(Input::has("menu") ? 'layouts.blank':'layouts.main' )
@section('titulo') {{ $configuraciongeneral[0] }} @stop
@section('estilos')
    <!-- DatePicker -->
    {{ HTML::style('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}
    <!-- Chosen -->
    {{ HTML::style('plugins/chosen/chosen.css') }}
    <!-- Selective -->
    {{ HTML::style('plugins/selective/selectize.bootstrap3.css') }}
    <!-- ColorBox-->
    {{ HTML::style('plugins/colorbox/colorbox.css') }}
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <!-- DatePicker -->
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') }}">
    </script>
    <!-- Chosen -->
    <script type="text/javascript" src="{{ asset('plugins/chosen/chosen.jquery.min.js') }}"></script>
    <!-- Selective -->
    <script type="text/javascript" src="{{ asset('plugins/selective/selectize.min.js') }}"></script>
    <!-- ColorBox-->
    {{ HTML::script('plugins/colorbox/jquery.colorbox.js') }}
    <script>
        $(document).ready(function() {
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
            @if (isset($validarjs))
                //Validacion
                jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio.",
                remote: "Por favor, rellena este campo.",
                email: "Por favor, escribe una dirección de correo válida",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número entero válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                accept: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
                });
                $('form').validate({
                rules: {
                @foreach ($validarjs as $key)
                    {!! $key !!},
                @endforeach
                /*
                clave: {
                minlength: 3,
                maxlength: 15,
                required: true
                },
                valor: {
                minlength: 3,
                maxlength: 15,
                required: true
                }
                */
                },
                highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
                } else {
                error.insertAfter(element);
                }
                }
                @if (isset($popup))
                    ,
                    submitHandler : function(form) {
                    //do something here
                    var frm = $('#form1');
                    frm.submit(function (ev) {
                    ev.preventDefault();
                    //$("#divconsulta").html($("#gif_cargando").html());
                    //$("#gif_cargando")
                    $.ajax({
                    type: frm.attr('method'),
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    statusCode: {
                    404: function() {
                    toastr["error"]("Se produjo un error al consultar los datos. 1 :(");
                    //bloquearpantalla(0);
                    },
                    401: function() {
                    toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    //bloquearpantalla(0);
                    }
                    //$("#divconsulta").html("Se produjo un error al consultar los datos. 4 :( ");
                    },
                    error: function(objeto, opciones, quepaso){
                    //setTimeout(function(){ajaxres.abort();}, 100);
                    //bloquearpantalla(0);
                    //$(tr).attr( "class","danger" );
                    //$("#divconsulta").html(quepaso);
                    toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {
                    console.log(data);
                    toastr[data[0]](data[1]);
                    parent.jQuery.colorbox.close();
                    }
                    });
                    //bloquearpantalla(0);
                    });
                    }
                @endif
                });
            @endif
            $(".chosen-select").chosen();
            //Fin Validacion
            $('.selective-normal').selectize();
            $('.selective-tags').selectize({
                plugins: ['remove_button'],
                persist: false,
                create: true,
                render: {
                    item: function(data, escape) {
                        return '<div>"' + escape(data.text) + '"</div>';
                    }
                },
                onDelete: function(values) {
                    //return confirm(values.length > 1 ? 'Esta seguro de borrar la selección ' + values.length : '');
                    return confirm('Esta seguro de borrar la selección?');
                }
            });
        });
        //==============================================
        @foreach ($objetos as $i => $campos)
            @php
                $ruta = explode('|', $campos->AccionObjeto);
            @endphp
            @if ($campos->Tipo == 'select' && $campos->AccionObjeto != 'Null' & count($ruta)>0)
                $("#{{ $campos->Nombre }}").change(function() {
                @if (count($ruta) && $ruta[0] == 'nuevo')
                    return cargarobj($(this), this, "{{ $ruta[1] }}", "{{ $ruta[2] }}")
                @endif
                });
            @endif
        @endforeach

        function cargarobjnormal(obj, objsub, ruta, rutaajax) {
            //console.log(this.value);
            if (objsub.value == '0') {
                obj.empty();
                obj.append("<option value='0'>Espere un momento...</option>");
                var win = window.open(ruta);
                var pollTimer = window.setInterval(function() {
                    if (win.closed !== false) { // !== is required for compatibility with Opera
                        window.clearInterval(pollTimer);
                        //var ruta = rutaajax;
                        $.get(rutaajax, function(data) {
                            //console.log(data);
                            obj.empty();
                            //Nuevo...
                            obj.append("<option value>Escoja opción...</option>");
                            obj.append("<option value>Nuevo...</option>");
                            $.each(data, function(key, element) {
                                obj.append("<option value='" + key + "'>" + element + "</option>");
                            });
                            $(obj).trigger("chosen:updated");
                        });
                    }
                }, 200);
            }
        }
        function cargarobj(obj, objsub, ruta, rutaajax) {
            //console.log(this.value);
            console.log(objsub.value);
            if (objsub.value == '0') {
                obj.empty();
                obj.append("<option value='0'>Espere un momento...</option>");
                $.colorbox({
                    href: ruta+"?menu=si",
                    fixed: true,
                    iframe: true,
                    innerWidth: screen.width - (screen.width * 0.20),
                    innerHeight: screen.height - (screen.height * 0.40),
                    onClosed:function(){
                        $.get(rutaajax, function(data) {
                            //console.log(data);
                            obj.empty();
                            //Nuevo...
                            obj.append("<option value>Escoja opción...</option>");
                            obj.append("<option value='0'>Nuevo...</option>");
                            $.each(data, function(key, element) {
                                obj.append("<option value='" + key + "'>" + element + "</option>");
                            });
                            $.colorbox.close();
                            $(obj).trigger("chosen:updated");
                        });
                        
                    }
                });
            }
        }

    </script>
@stop
@section('content')
    <div class="panel panel-body">
        <div class="panel-heading">
            <h3>
                @if ($configuraciongeneral[2] == 'crear')
                    Nuevo Registro - {!! $configuraciongeneral[0] !!}
                @else
                    Editar Registro - {!! $configuraciongeneral[0] !!}
                @endif
            </h3>
            <p>
                <a href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary "><i
                        class="fa fa-list"></i>Todos</a>
                @if ($permisos->crear == 'SI')
                    <a href="{{ URL::to($configuraciongeneral[1]) . '/create' }}" class="btn btn-default ">
                        <i class="fa fa-plus"></i> Nuevo</a>
                @endif
            </p>
        </div><!-- panel-heading -->
        <div class="panel-body">
            <!-- Mensajes y Alertas -->
            @if (Session::has('message'))
                <p>
                    <br>
                    <script>
                        $(document).ready(function() {
                            //toastr.succes("{{ Session::get('message') }}");
                            toastr["success"]("{{ Session::get('message') }}");
                            //$.notify("{{ Session::get('message') }}","success");
                        });

                    </script>
                <div class="alert alert-info alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×
                    </button>{{ Session::get('message') }}
                </div>
                </p>
            @elseif (Session::has('error'))
                <p>
                    <br>
                    <script>
                        $(document).ready(function() {
                            //toastr.succes("{{ Session::get('error') }}");
                            toastr["error"]("{{ Session::get('error') }}");
                            //$.notify("{{ Session::get('error') }}","success");
                        });

                    </script>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×
                    </button>{{ Session::get('error') }}
                </div>
                </p>
            @endif
            @if ($errors->all())
                <p>
                    <br>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            toastr["error"]($("#diverror").html());
                        });

                    </script>
                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Error!</h4>
                    <div id="diverror">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                </p>
            @endif
            <!-- INICIO FORMULARIO -->
            <p>
                <br>
                @if ($configuraciongeneral[2] == 'crear')
                    {!! Form::open(['url' => $configuraciongeneral[1], 'role' => 'form', 'id' => 'form1', 'class' => 'form-horizontal']) !!}
                @else
                    {!! Form::model($tabla, ['route' => [$configuraciongeneral[1] . '.update', $tabla->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'form1']) !!}
                @endif


                @foreach ($objetos as $i => $campos)
                    <div class="form-group">
                        {!! Form::label($campos->Nombre, $campos->Descripcion . ':', ['class' => 'col-lg-3 control-label']) !!}
                        <div class="col-lg-8">
                            @if ($campos->Tipo == 'date')
                                <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                {!! Form::text($campos->Nombre, null, ['data-placement' => 'top', 'data-toggle' => 'tooltip', 'class' => 'form-control datefecha', 'placeholder' => $campos->Descripcion, $campos->Nombre, 'readonly' => 'readonly']) !!}
                            @elseif($campos->Tipo=="datetext")
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    {!! Form::text($campos->Nombre, null, [
    'data-placement' => 'top',
    'data-toogle' => 'tooltip',
    'class' => 'form-control datefecha',
    'placeholder' => $campos->Descripcion,
    $campos->Nombre,
    'readonly' => 'readonly',
]) !!}
                                </div>
                            @elseif($campos->Tipo=="select")
                                {!! Form::select($campos->Nombre, $campos->Valor, (Input::old($campos->Nombre) ? Input::old($campos->Nombre) : $campos->ValorAnterior != 'Null') ? $campos->ValorAnterior : Input::old($campos->Nombre), ['class' => 'form-control ' . $campos->Clase]) !!}
                            @elseif($campos->Tipo=="select2")
                                {!! Form::select($campos->Nombre, $campos->Valor, $campos->ValorAnterior, ['class' => 'form-control ' . $campos->Clase]) !!}
                            @elseif($campos->Tipo=="select-multiple")
                                {!! Form::select($campos->Nombre, $campos->Valor, (Input::old($campos->Nombre) ? Input::old($campos->Nombre) : $campos->ValorAnterior != 'Null') ? $campos->ValorAnterior : Input::old($campos->Nombre), ['class' => 'form-control ' . $campos->Clase, 'multiple' => 'multiple']) !!}
                            @elseif($campos->Tipo=="file")
                                {!! Form::file($campos, ['class' => 'form-control']) !!}
                            @elseif($campos->Tipo=="textdisabled")
                                {!! Form::text($campos, Input::old($campos), ['class' => 'form-control', 'placeholder' => 'Ingrese ' . $camposcaption[$i], 'readonly']) !!}
                            @elseif($campos->Tipo=="textarea")
                                {!! Form::textarea($campos->Nombre, Input::old($campos->Valor), ['class' => 'form-control ' . $campos->Clase, 'placeholder' => 'Ingrese ' . $campos->Descripcion]) !!}

                            @elseif($campos->Tipo=="password")
                                {!! Form::password($campos->Nombre, ['class' => 'form-control', 'placeholder' => 'Ingrese ' . $campos->Nombre]) !!}
                            @else
                                {!! Form::text($campos->Nombre, Input::old($campos->Valor), ['class' => 'form-control', 'placeholder' => 'Ingrese ' . $campos->Descripcion]) !!}
                            @endif
                        </div>
                    </div>

                @endforeach
                @if (isset($popup))
                    <input type="hidden" value="popup" name="txtpopup" />
                    <!--else -->
                    <!--input type="hidden" value="" name="txtpopup"/-->
                @endif
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'style' => 'float:right']) !!}

                {!! Form::close() !!}
                <!-- FIN FORMULARIO -->
            </p>

        </div><!-- panel-body -->
    </div><!-- panel panel-body -->


@stop
