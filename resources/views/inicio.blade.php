@extends('layouts.main')

@section('content')
<div class="panel panel-body text-center">
<div class="panel-heading">
    <h3>{{ config('app.name', 'Laravel') }}</h3>
</div>
<div class="panel-body">
    <p>Bienvenido al sistema.</p>                                    
</div>
</div>
@if ($errors->all())
                <p>
                    <br>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            toastr["error"]($("#diverror").html());
                        });

                    </script>
                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Error!</h4>
                    <div id="diverror">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                </p>
            @endif
<div class="row">
                            <div class="col-md-3">
                                <div class="panel panel-warning panel-colorful media middle pad-all">
                                    <div class="media-left">
                                        <div class="pad-hor">
                                            <i class="fa fa-truck icon-3x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-semibold">241</p>
                                        <!-- <p class="mar-no">Maquinarias OOPP</p> -->
                                        <p class="mar-no">Item 1</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-info panel-colorful media middle pad-all">
                                    <div class="media-left">
                                        <div class="pad-hor">
                                            <i class="fa fa-bus icon-3x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-semibold">241</p>
                                        <!-- <p class="mar-no">Riego y drenaje </p>-->
                                        <p class="mar-no">Item 2 </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-mint panel-colorful media middle pad-all">
                                    <div class="media-left">
                                        <div class="pad-hor">
                                            <i class="fa fa-car icon-3x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-semibold">241</p>
                                        <!-- <p class="mar-no">Vehículos</p>-->
                                        <p class="mar-no">Item 3 </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-danger panel-colorful media middle pad-all">
                                    <div class="media-left">
                                        <div class="pad-hor">
                                            <i class="fa fa-road icon-3x"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <p class="text-2x mar-no text-semibold">241</p>
                                        <p class="mar-no">Otros</p>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
@endsection
