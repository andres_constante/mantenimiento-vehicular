@extends('layouts.auth')

@section('content')
<!-- LOGIN FORM -->
        <!--===================================================-->
        <div class="cls-content">
            <div class="cls-content-sm panel">
                <div class="panel-body">
                    <div class="mar-ver pad-btm">
                        <h1 class="h3">{{ config('app.name', 'Laravel') }}</h1>
                        <p>Restaurar contraseña</p>
                    </div>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Ingrese su correo por favor" name="email" autofocus value="{{ old('email') }}"  required autocomplete="email">
                            @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>                        
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Procesar</button>
                    </form>
                </div>
                <div class="pad-all">
                    <a href="{{ route('home') }}" class="btn-link mar-rgt">Inicio</a>
                    <a href="{{ route('register') }}" class="btn-link mar-lft">Registrarse</a>                    
                </div>
            </div>
        </div>
@endsection
