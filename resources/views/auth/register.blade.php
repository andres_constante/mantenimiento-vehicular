@extends('layouts.auth')

@section('content')
<!-- REGISTRATION FORM -->
        <!--===================================================-->
        <div class="cls-content">
            <div class="cls-content-lg panel">
                <div class="panel-body">
                    <div class="mar-ver pad-btm">
                        <h1 class="h3">{{ config('app.name', 'Laravel') }}</h1>
                        <p>Crear una cuenta en el sistema.</p>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="Nombres" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="E-mail" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                
                                <div class="form-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Confirmar Password" name="password_confirmation" required autocomplete="new-password">                                    
                                </div>
                            </div>
                        </div>
                        {{-- 
                        <div class="checkbox pad-btm text-left">
                            <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
                            <label for="demo-form-checkbox">I agree with the <a href="#" class="btn-link text-bold">Terms and Conditions</a></label>
                        </div>
                        --}}
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Registrarse</button>
                    </form>
                </div>
                <div class="pad-all">
                    <a href="{{ route('home') }}" class="btn-link mar-rgt">Inicio</a>
                    <a href="{{ route('login') }}" class="btn-link mar-rgt text-bold">Tienes una cuenta?</a>
                            
                </div>
            </div>
        </div>
@endsection
