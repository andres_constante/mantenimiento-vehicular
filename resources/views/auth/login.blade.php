@extends('layouts.auth')

@section('content')
<!-- LOGIN FORM -->
        <!--===================================================-->
        <div class="cls-content">
            <div class="cls-content-sm panel">
                <div class="panel-body">
                    <div class="mar-ver pad-btm">
                        <h1 class="h3">{{ config('app.name', 'Laravel') }}</h1>
                        <p>Por favor ingrese su Email y Contraseña para empezar.</p>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="correo@correo.com" name="email" autofocus value="{{ old('email') }}"  required autocomplete="email">
                            @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" name="password" required autocomplete="current-password">
                            @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="checkbox pad-btm text-left">
                            <input class="magic-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="demo-form-checkbox">Recordarme</label>
                        </div>
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Entrar</button>
                    </form>
                </div>
        
                <div class="pad-all">
                    <a href="{{ route('password.request') }}" class="btn-link mar-rgt">Olvidó su contraseña?</a>
                    <a href="{{ route('register') }}" class="btn-link mar-lft">Registrarse</a>                    
                </div>
            </div>
        </div>
@endsection
