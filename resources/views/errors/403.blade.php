@extends('layouts.main')
@section ('titulo')  Página no Autorizada @stop
@section ('content')
<div id="container" class="cls-container">
<div class="cls-content">
            <h1 class="error-code text-info">403</h1>
            <p class="h4 text-uppercase text-bold">Página no encontrada!</p>
            <div class="pad-btm">
                No tienes permiso para acceder a esta página.<br><br>
            <i>{{ Request::url() }}</i>
            </div>
            <hr class="new-section-sm bord-no">
            <div class="pad-top"><a class="btn btn-primary" href="{{ route("home") }}">Inicio</a></div>
        </div>
</div>
@stop
