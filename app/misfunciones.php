<?php
//use Request;

use App\Models\MenuModel;
use App\Models\AuditoriaModel;
//use Auth;
//use App\PermisosUsuariosModel;
function Auditoria($accion)
{
    $historial = new AuditoriaModel();
    $historial->idusuario = Auth::user()->id;
    $historial->urlmenu = Request::getRequestUri();
    $historial->accion = $accion;
    $historial->ip = Request::getClientIp();
    $historial->nompc = Request::getHost();

    $historial->save();
}
function getMenu($admin = "NO", $idusuario = 0, $todo = 0, $tipobus = "usuario")
{
    if (Auth::user()->id_tipo_usuario == 1) {
        $menu = MenuModel::menus($admin, $idusuario, $todo, $tipobus);
    } else {
        if($admin=="SI")
            return array();
        $menu = MenuModel::menus($admin, Auth::user()->id, Auth::user()->id, $tipobus);
        //$menu = getmenu("NO", Auth::user()->id, Auth::user()->id);        
    }
    //$menu = MenuModel::menus($admin, $idusuario, $todo, $tipobus);

    return $menu;
    abort(404);
}
function show($objeto, $i = 1)
{
    echo "<pre>";
    print_r($objeto);
    if ($i == 1)
        die();
}
