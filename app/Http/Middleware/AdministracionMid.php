<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
class AdministracionMid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //if($request->session()->has('users')){
            if (Auth::user()->id_tipo_usuario != 1)
            return redirect("home")
                ->withErrors(array("No tiene el permiso para este enlace.",$request->url()));
        //}
        return $next($request);
    }
}
