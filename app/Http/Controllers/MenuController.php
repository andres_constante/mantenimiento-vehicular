<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\MenuModel;
use App\misclases\permisosadmin;
class MenuController extends Controller
{
    //
    var $configuraciongeneral = array("Menú de Opciones", "menu", "index", 6 => 'menuajax');
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Nombre del Menú","Nombre":"menu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" ,"AccionObjeto" :"Null"}, 
        {"Tipo":"text","Descripcion":"Ruta / URL","Nombre":"ruta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" ,"AccionObjeto" :"Null"},
        {"Tipo":"text","Descripcion":"Icono","Nombre":"icono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" ,"AccionObjeto" :"Null"},
        {"Tipo":"select","Descripcion":"Nivel","Nombre":"nivel","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" ,"AccionObjeto" :"Null"},
        {"Tipo":"select-ajax","Descripcion":"ID Padre","Nombre":"idmain","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" ,"AccionObjeto" :"Null"},
        {"Tipo":"text","Descripcion":"Orden","Nombre":"orden","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI","AccionObjeto" :"Null" },
        {"Tipo":"text","Descripcion":"Adicional","Nombre":"adicional","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" ,"AccionObjeto" :"Null" },
        {"Tipo":"select","Descripcion":"Visible","Nombre":"visible","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI","AccionObjeto" :"Null" }
                  ]';
    var $escoja = array(null => "Escoja opción...");
    var $nuevo = array("0" => "Nuevo...");
    //https://jqueryvalidation.org/validate/
    /*var $validarjs = array(
        "name" => "name: {
                            required: true
                        }",
        "email" => "email: {
                            required: true
                        }",
        "password" => "password: {
                            required: true
                        }",
        "password_confirmation" => "password_confirmation: {
                            required: true
                        }",
        "id_tipo_usuario" => "id_tipo_usuario: {
                            required: true
                        }"

    );*/
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AdministracionMid'); //->except(['index','show']);;
    }
    public function querymain($main = "main")
    {
        switch ($main) {
            case 'main':
                # code...
                return MenuModel::select('*')
                    ->where("estado", "ACT");
                break;
            case 'permisos':
                if (Auth::user()->id_tipo_usuario == 1) {
                    $permisos = new permisosadmin;
                    return $permisos;
                }
                return PermisosUsuariosModel::join("ad_menu as a", "a.id", "=", "ad_menuusuario.idmenu")
                    ->select("ad_menuusuario.*")
                    ->where("ad_menuusuario.idusuario", Auth::user()->id)
                    ->where("a.ruta", 'like', $this->configuraciongeneral[1])
                    ->first();
            default:
                # code...
                break;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $id = 0;
        if ($request->has('id')) {
            //
            $id = intval($request["id"]);
        }
        //dd($input);
        //
        //$objetos=json_decode($this->objetos);
        //$objetos[6]->Valor= $this->escoja + (array)$tiposolicitudes;
        //$objetos[6]->Nombre=$objetos[6]->Nombre."[]";
        $main = MenuModel::select(DB::raw("case when nivel=2 then concat('2 --> ',menu) 
                else case when nivel=3 then concat('3 ------> ',menu) else menu end end as menu"), "id")
            ->where(function ($query) use ($id) {
                $query->where("id", "<>", 0);
                if ($id != 0)
                    $query->where("id", $id)->orwhere("idmain", $id);
            })
            ->orderBy("idmain", "asc")
            ->orderBy("orden", "asc")
            ->orderBy("id", "asc")
            ->pluck("menu", "id")->all();
        $objetos = json_decode($this->objetos);
        $objetos[4]->Valor = $this->escoja + $main;
        $objetos[4]->ValorAnterior = $id;
        $objetos[3]->Valor = array(1 => "1", 2 => "2", 3 => "3");
        $objetos[7]->Valor = array("SI" => "SI", "NO" => "NO");
        //show($objetos);        
        $objetos = array_values($objetos);
        /*
        $tabla= MenuModel::join("ad_menu as b","ad_menu.idmain","=","b.id") 
                    ->select("ad_menu.*" ,"b.menu as idmainlista")                    
                    ->orderBy("ad_menu.idmain", "asc")
	                ->orderBy("ad_menu.orden", "asc")
	                ->orderBy("ad_menu.id", "asc")
                    ->get(); 
                    */
        $tabla = MenuModel::orderBy("idmain", "asc")
            ->where(function ($query) use ($id) {
                $query->where("id", "<>", 0);
                if ($id != 0)
                    $query->where("id", $id)->orwhere("idmain", $id);
            })
            ->orderBy("orden", "asc")
            ->orderBy("id", "asc")
            ->get(); //->paginate(500);
        $permisos = $this->querymain("permisos");
        return view('permisosmenu.menuindex', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "urlmenu" => "permisostipousu",
            "permisos" => $permisos
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$input = $request->all();
        //return $input;
        $datosenvio = Input::get("datosenvio");
        //return $datosenvio;
        $id = trim($datosenvio[0]);

        //return $datosenvio;

        // Validacion
        $objetos = json_decode($this->objetos);
        $objetos = array_values($objetos);
        //return $objetos;
        foreach ($objetos as $keycam => $valuecam) {
            if ($datosenvio[$keycam + 1] == "-" || $datosenvio[$keycam + 1] == "") {
                if ($valuecam->Requerido == "SI") {
                    return "Por Favor Ingrese correctamente en el campo " . $valuecam->Nombre;
                }
            };
            if ($valuecam->Nombre == "menu" && $id == "-") {
                $verificarexisteid = MenuModel::where("menu", trim($datosenvio[$keycam + 1]))->first();
                if ($verificarexisteid)
                    return "El menú '" . trim($datosenvio[$keycam + 1]) . "' ya existe.";
            }
        }
        //

        $verificarexisteid = MenuModel::find($id);


        if (!$verificarexisteid) {
            $guardar = new MenuModel;
        } else {
            $guardar = MenuModel::find($id);
        }

        foreach ($objetos as $keycam => $valuecam) {
            $dato = trim($datosenvio[$keycam + 1]);
            if ($dato == "-")
                $dato = "";
            $nombre = $valuecam->Nombre;
            if ($nombre == "idmain" && trim($dato) == "") {

                $guardar->$nombre = 0;
            } else
                $guardar->$nombre = $dato;
        }

        $guardar->save();
        //return $guardar;               
        return $guardar->id;
    }
}
