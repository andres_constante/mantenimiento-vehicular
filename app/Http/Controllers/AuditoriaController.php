<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\AuditoriaModel;
use App\misclases\permisosadmin;
class AuditoriaController extends Controller
{
    //
    var $configuraciongeneral = array("Auditoría", "auditoria", "index", 6 => 'auditoriaajax');
    var $objetos = '[  
                    {"Tipo":"select","Descripcion":"Usuario","Nombre":"usuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" ,"AccionObjeto" :"Null"}, 
                  {"Tipo":"text","Descripcion":"Acción","Nombre":"accion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                  {"Tipo":"text","Descripcion":"URL","Nombre":"urlmenu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                  {"Tipo":"text","Descripcion":"IP","Nombre":"ip","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                  {"Tipo":"text","Descripcion":"PC","Nombre":"nompc","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                  {"Tipo":"text","Descripcion":"Fecha","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"}
                  ]';
    var $escoja = array(null => "Escoja opción...");
    var $nuevo = array("0" => "Nuevo...");
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(

    );
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AdministracionMid'); //->except(['index','show']);;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objetos = json_decode($this->objetos);
        $tabla = [];
        $permisos = $this->querymain("permisos");
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "permisos" => $permisos,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }
    public function querymain($main = "main")
    {
        switch ($main) {
            case 'main':
                # code...
                return AuditoriaModel::join("users as a","a.id","=","ad_auditoria.idusuario")
                	->select('ad_auditoria.*','a.name as usuario');                    
                break;
            case 'permisos':
                if (Auth::user()->id_tipo_usuario == 1) {
                    $permisos = new permisosadmin;
                    return $permisos;
                }
                return PermisosUsuariosModel::join("ad_menu as a", "a.id", "=", "ad_menuusuario.idmenu")
                    ->select("ad_menuusuario.*")
                    ->where("ad_menuusuario.idusuario", Auth::user()->id)
                    ->where("a.ruta", 'like', $this->configuraciongeneral[1])
                    ->first();
            default:
                # code...
                break;
        }
    }
    public function auditoriaajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'usuario',
            2 => 'accion',
            3 => 'urlmenu',
            4 => 'ip',
            5 => 'nompc',
            6 => 'created_at',
            7 =>'acciones'
        );
        $maintb = $this->querymain();
        $totalData = $maintb->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = $maintb->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $postscount =  $maintb->where('ad_auditoria.ip', 'LIKE', "%{$search}%")
                ->orWhere("ad_auditoria.nompc", 'LIKE', "%{$search}%")
                ->orWhere("a.name", 'LIKE', "%{$search}%");
            $totalFiltered = $postscount->count();
            $posts = $postscount->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = link_to_route(
                    $this->configuraciongeneral[1].'.show',
                    '',
                    array(Crypt::encrypt($post->id)),
                    array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')
                );
                $permisos = $this->querymain("permisos");
                $edit = "";
                
                $dele = "";
                
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                $nestedData = array();
                foreach ($columns as $key => $value) {
                    # code...
                    if ($value == "acciones")
                        $nestedData['acciones'] = $aciones;
                    elseif($value == "created_at" || $value == "updated_at")
                        $nestedData[$value] = date("Y-m-d H:i:s",strtotime($post->$value));
                   	else
                        $nestedData[$value] = $post->$value;
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id = Crypt::decrypt($id);
        $objetos = json_decode($this->objetos);        
        //show($objetos);
        $tabla = $this->querymain()->where("ad_auditoria.id", $id)->first();
        //show($tabla);
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
            //"detalle"=>1 Al enviar este parametro cambiar a get en vez de first en query
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->index();
    }
}