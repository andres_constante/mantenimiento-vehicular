<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\AsignarPermisosModel;
use App\Models\UsuariosModel;
use App\Models\MenuModel;
use App\Models\PerfilPermisosModel;
use App\misclases\permisosadmin;
class AsignarPermisosController extends Controller
{
    //    
    var $configuraciongeneral = array("Asignar Permisos", "asignarpermisos", "crear", "permisostipousu", 6 => 'asignarpermisosajax', 4 => 'llamarpermisoscrud');
    var $objetos = '[
        {"Tipo":"select2","Descripcion":"Usuario","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI","AccionObjeto" :"Null" }
                  ]';
    var $escoja = array(null => "Escoja opción...");
    var $nuevo = array("0" => "Nuevo...");
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AdministracionMid'); //->except(['index','show']);;
    }
    public function querymain($main = "main")
    {
        switch ($main) {
            case 'main':
                # code...
                return MenuModel::select('*')
                    ->where("estado", "ACT");
                break;
            case 'permisos':
                if (Auth::user()->id_tipo_usuario == 1) {
                    $permisos = new permisosadmin;
                    return $permisos;
                }
                return PermisosUsuariosModel::join("ad_menu as a", "a.id", "=", "ad_menuusuario.idmenu")
                    ->select("ad_menuusuario.*")
                    ->where("ad_menuusuario.idusuario", Auth::user()->id)
                    ->where("a.ruta", 'like', $this->configuraciongeneral[1])
                    ->first();
            default:
                # code...
                break;
        }
    }
    public function index()
    {
        //
        $main = UsuariosModel::orderby("name")->pluck("name", "id")->all();
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $main;
        //show($objetos);
        //$objetos=array_values($objetos);
        $tabla = array(); //->paginate(500)
        $permisos = $this->querymain("permisos");
        return view('permisosmenu.createindex', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            //"checkmenu"=>"si",
            "validarjs" => array(),
            "permisos" => $permisos
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idbus = Input::get("id");
        $create = $this->index()->getData(); //->data();        
        //$create["configuraciongeneral"][2] = "editar";
        $create["objetos"][0]->ValorAnterior = $idbus;
        //dd($create);
        $param = [
            "checkmenu" => "si",
            "menutodo" => "si",
            "idbus" => $idbus
        ];
        $res = array_merge($create, $param);
        //dd($res);
        return view('permisosmenu.createindex', $res);
    }
    public function permisostipousu()
    {
        $id = Input::get("id");
        $menutodo = intval(Input::get("menutodo"));
        $tipobus = Input::get("tipobus");
        $ruta = Input::get("ruta");
        $submit = 0;
        //dd($tipobus);
        //////////////////////////////////////////////////////////////
        $menu = getmenu("NO", $id, 0, $tipobus); //getmenu("NO",$id);
        //dd($menu);
        if ($menutodo == 1)
            $submit = 1;
        else
            $menu = getmenu("NO", $id, $id, $tipobus);
        /*
        if($tipobus=="usuario")
            {
              $tablapermisos= MenuModel::whereIn("id",function($query) use($id){
                $query->select('idmenu')
                ->from(with(new AsignarPermisosModel)->getTable())                
                ->where('idusuario', $id);
              })->orderby("orden")->get();
            }else{
              $tablapermisos= MenuModel::whereIn("id",function($query) use($id){
                $query->select('idmenu')
                ->from(with(new PerfilPermisosModel)->getTable())                
                ->where('idperfil', $id);
              })->orderby("orden")->get();
            }
        */
        //dd($tabla->toarray());
        //dd($menu);
        return view('tree.maintree', [
            "menu" => $menu,
            "idsi" => $id,
            "ruta" => $ruta,
            "submit" => $submit
        ]);
    }
    public function menutree()
    {
        $menu = getmenu();
        //dd($menu);
        return view('tree.maintree', [
            "menu" => $menu
        ]);
    }
    public function guardar($id, $request)
    {
        $input = $request->all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new AsignarPermisosModel;
            $msg = "Registro Creado Exitosamente...!";
        } else {
            $ruta .= "/$id/edit";
            $guardar = AsignarPermisosModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
        }
        $arrapas = array();

        $validator = Validator::make($input, AsignarPermisosModel::rules());

        if ($validator->fails()) {
            //die($ruta);
            if (!Input::has("idmenumain") && Input::has("name")) {
                $del = AsignarPermisosModel::where('idusuario', '=', Input::get('name'))->delete();
                Session::flash('message', "Todos los permisos fueron eliminados para el Tipo de Usuario");
                return redirect($this->configuraciongeneral[1]);
            } else {
                return redirect("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }
        } else {
            //die($ruta);
            $sw = 0;
            $permimenu = Input::get('idmenumain');
            $tipo = Input::get('name');
            /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
            //DB::table('menutipousuario')->where('idtipousuario', '=', $tipo)->delete();
            $del = AsignarPermisosModel::where('idusuario', '=', $tipo)->delete();
            //print_r($permimenu);
            //                        if(is_array($permimenu))
            //                      {
            foreach ($permimenu as $valor) {
                echo "Menu:  $valor <br>";
                $permisos = new AsignarPermisosModel;
                $permisos->idusuario  = $tipo;
                $permisos->idmenu     = $valor;
                /*$verpa= DB::table('menutipousuario')->where(function($query)use($valor,$tipo){
                                    $query->where("idtipousuario",$tipo)->where("idmenu",$valor);*/
                $verpa = AsignarPermisosModel::where(function ($query) use ($valor, $tipo) {
                    $query->where("idusuario", $tipo)->where("idmenu", $valor);
                })->first();
                if (!$verpa) {
                    $permisos->save();
                }
                $sw = 1;
                /*Ver padre del hijo*/
                //$verpa= DB::table('menu')->where("id",$valor)->first();
                $verpa = MenuModel::where("id", $valor)->first();
                $idpa = intval($verpa->idmain);
                $verpa = AsignarPermisosModel::where(function ($query) use ($idpa, $tipo) {
                    $query->where("idusuario", $tipo)->where("idmenu", $idpa);
                })->first();

                if (!$verpa && $idpa != 0) {
                    $permisos = new AsignarPermisosModel;
                    $permisos->idusuario  = $tipo;
                    $permisos->idmenu     = $idpa;

                    $permisos->save();
                }
            }
            //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
            // redirect
            Auditoria("Asignación de Permisos por Usuario" . " - ID: " . $tipo);
            Session::flash('message', $msg);
            return redirect($this->configuraciongeneral[1]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0, $request);
        $input = $request->all();
        dd($input);
        //return $this->guardar(0);
    }
    function llamarpermisoscrud(Request $request)
    {
        $request = $request->all();
        //dd($request);
        $id = $request["id"];
        $tipobus = $request["tipobus"];
        //dd($tipobus);
        if ($tipobus == "usuario")
            $res = AsignarPermisosModel::join("ad_menu as men", "men.id", "=", "ad_menuusuario.idmenu")
                ->select("ad_menuusuario.*", DB::raw("case when men.nivel=2 then concat('2 --> ',men.menu) 
            else case when men.nivel=3 then concat('3 ------> ',men.menu) else men.menu end end as menu"))
                ->where(function ($query) use ($id) {
                    $query->where("ad_menuusuario.idusuario", $id);
                })
                ->orderBy("men.idmain", "asc")
                ->orderBy("men.orden", "asc")
                ->orderBy("men.id", "asc")
                ->get();
        else
            $res = PerfilPermisosModel::join("ad_menu as men", "men.id", "=", "ad_perfilpermisos.idmenu")
                ->select("ad_perfilpermisos.*", DB::raw("case when men.nivel=2 then concat('2 --> ',men.menu) 
    else case when men.nivel=3 then concat('3 ------> ',men.menu) else men.menu end end as menu"))
                ->where(function ($query) use ($id) {
                    $query->where("ad_perfilpermisos.idperfil", $id);
                })
                ->orderBy("men.idmain", "asc")
                ->orderBy("men.orden", "asc")
                ->orderBy("men.id", "asc")
                ->get();

        //dd($res->toarray());
        $sino = array("SI" => "SI", "NO" => "NO");
        return view("permisosmenu.crudper", ["tabla" => $res, "sino" => $sino, "tipobus" => $tipobus, "idusuario" => $id]);
    }
    function llamarpermisoscrudajax(Request $request)
    {
        $input = $request->all();
        if (array_key_exists("idpermiso", $input)) {
            $id = intval($input["idpermiso"]);
            if($input["tipobus"]=="usuario")
                $permisos = AsignarPermisosModel::find($id);
            else
                $permisos = PerfilPermisosModel::find($id);
            $permisos->crear = $input["crear"];
            $permisos->editar = $input["editar"];
            $permisos->eliminar = $input["eliminar"];
            $permisos->save();
            Auditoria("Asignación de Permisos CRUD" . " - ID: " . $id);
        }
        //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
        // redirect        
        //dd($input);
        echo '<div class="alert alert-info">
        <strong>Se guardó exitosamente:</strong> ' . $input["crear"] . " - " . $input["editar"] . " - " . $input["eliminar"] . '</div>';
        return $this->llamarpermisoscrud($request);
    }
}
