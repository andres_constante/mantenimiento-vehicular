<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\SysConfigModel;
use App\misclases\permisosadmin;
class SysConfigController extends Controller
{
    //
    var $configuraciongeneral = array("Variables de Configuración", "config", "index", 6 => 'configajax');
    var $objetos = '[  
                    {"Tipo":"text","Descripcion":"Variable","Nombre":"campo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                    {"Tipo":"text","Descripcion":"Valor","Nombre":"valor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"},
                    {"Tipo":"text","Descripcion":"Valor Adicional","Nombre":"valorsub","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","AccionObjeto" :"Null"}
                  ]';
    var $escoja = array(null => "Escoja opción...");
    var $nuevo = array("0" => "Nuevo...");
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "campo" => "campo: {
                            required: true
                        }",
         "valor" => "valor: {
			required: true
			}",
         "valorsub" => "valorsub: {
			required: true
			}"

    );
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AdministracionMid'); //->except(['index','show']);;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objetos = json_decode($this->objetos);        
        $tabla = [];
        $permisos = $this->querymain("permisos");
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "permisos" => $permisos,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }
    public function querymain($main = "main")
    {
        switch ($main) {
            case 'main':
                # code...
                return SysConfigModel::select('*')
                    ->where("estado", "ACT");
                break;
            case 'permisos':
                if (Auth::user()->id_tipo_usuario == 1) {
                    $permisos = new permisosadmin;
                    return $permisos;
                }
                return PermisosUsuariosModel::join("ad_menu as a", "a.id", "=", "ad_menuusuario.idmenu")
                    ->select("ad_menuusuario.*")
                    ->where("ad_menuusuario.idusuario", Auth::user()->id)
                    ->where("a.ruta", 'like', $this->configuraciongeneral[1])
                    ->first();
            default:
                # code...
                break;
        }
    }
    public function configajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'campo',
            2 => 'valor',
            3 => 'valorsub',
            4 =>'acciones'
        );
        $maintb = $this->querymain();
        $totalData = $maintb->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = $maintb->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $postscount =  $maintb->where('id', 'LIKE', "%{$search}%")
                ->orWhere("campo", 'LIKE', "%{$search}%")
                ->orWhere("valor", 'LIKE', "%{$search}%");
            $totalFiltered = $postscount->count();
            $posts = $postscount->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = link_to_route(
                    $this->configuraciongeneral[1].'.show',
                    '',
                    array(Crypt::encrypt($post->id)),
                    array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')
                );
                $permisos = $this->querymain("permisos");
                $edit = "";
                if ($permisos->editar == "SI")
                    $edit = link_to_route(
                        $this->configuraciongeneral[1].'.edit',
                        '',
                        array(Crypt::encrypt($post->id), "menu" => "no"),
                        array('class' => 'fa fa-pencil-square-o divpopup') //, 'onclick' => 'popup(this)')
                    );
                $dele = "";
                if ($permisos->eliminar == "SI") {
                    $dele = "<a href='#' onClick=eliminar('" . $post->id . "')><i class='fa fa-trash'></i></a>";
                    $dele .= '<div style="display: none;">
                        <form method="POST" action="'.$this->configuraciongeneral[1].'/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right">
                            <input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="' . csrf_token() . '">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                }

                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                $nestedData = array();
                foreach ($columns as $key => $value) {
                    # code...
                    if ($value == "acciones")
                        $nestedData['acciones'] = $aciones;
                    else
                        $nestedData[$value] = $post->$value;
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "permisos" => $this->querymain("permisos")
        ]);
    }
    public function guardar($id, $request)
    {
        //return $id;
        $input = $request->all();
        //return $input;
        $rutamain = $this->configuraciongeneral[1];
        $ruta = $rutamain;
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new SysConfigModel;
            $msg = "Registro Creado Exitosamente...!";
        } else {
            $ruta .= "/$id/edit";
            $guardar = SysConfigModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
        }        
        $arrapas = SysConfigModel::rules($id);
        $validator = Validator::make($input, $arrapas);

        if ($validator->fails()) {
            //die($ruta);
            return redirect("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            foreach ($input as $key => $value) {

                if ($key != "_method" && $key != "_token" && $key != "password_confirm") {                    
                        $guardar->$key = $value;
                }
            }            
            $guardar->save();
            Auditoria($this->configuraciongeneral[0]. ": ".$guardar->id."=>".$msg);
        }
        Session::flash('message', $msg);
        return redirect($rutamain);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id = Crypt::decrypt($id);
        $objetos = json_decode($this->objetos);        
        //show($objetos);
        $tabla = $this->querymain()->where("id", $id)->first();
        //show($tabla);
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
            //"detalle"=>1 Al enviar este parametro cambiar a get en vez de first en query
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
        $create = $this->create()->getData(); //->data();        
        $create["configuraciongeneral"][2] = "editar";
        //dd($create);        
        $id = Crypt::decrypt($id);
        $tabla = SysConfigModel::find($id);
        $res = array_merge($create, ["tabla" => $tabla]);
        return view('vistas.create', $res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla = SysConfigModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        Auditoria($this->configuraciongeneral[0]. ": ".$tabla->id."=>Eliminación");
        return redirect($this->configuraciongeneral[1]);
    }
}