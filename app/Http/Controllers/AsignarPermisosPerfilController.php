<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\AsignarPermisosModel;
use App\Models\UsuariosModel;
use App\Models\MenuModel;
use App\Models\PerfilModel;
use App\Models\PerfilPermisosModel;
use App\misclases\permisosadmin;
class AsignarPermisosPerfilController extends Controller
{
    //
    var $configuraciongeneral = array("Asignar Permisos Perfil", "asignarpfilpermisos", "crear", "permisostipousu", 6 => 'perfilpermisosasignarajax',4 => 'llamarpermisoscrud');
    var $objetos = '[
        {"Tipo":"select2","Descripcion":"Perfil","Nombre":"perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI","AccionObjeto" :"Null" }
                  ]';
    var $escoja = array(null => "Escoja opción...");
    var $nuevo = array("0" => "Nuevo...");
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AdministracionMid'); //->except(['index','show']);;
    }
    public function querymain($main = "main")
    {
        switch ($main) {
            case 'main':
                # code...
                return MenuModel::select('*')
                    ->where("estado", "ACT");
                break;
            case 'permisos':
                if (Auth::user()->id_tipo_usuario == 1) {
                    $permisos = new permisosadmin;
                    return $permisos;
                }
                return PermisosUsuariosModel::join("ad_menu as a", "a.id", "=", "ad_menuusuario.idmenu")
                    ->select("ad_menuusuario.*")
                    ->where("ad_menuusuario.idusuario", Auth::user()->id)
                    ->where("a.ruta", 'like', $this->configuraciongeneral[1])
                    ->first();
            default:
                # code...
                break;
        }
    }
    public function index()
    {
        //
        $main = PerfilModel::orderby("perfil")->pluck("perfil", "id")->all();
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $main;
        //show($objetos);
        //$objetos=array_values($objetos);
        $tabla = array(); //->paginate(500)
        $permisos = $this->querymain("permisos");
        return view('permisosmenu.createindex', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            //"checkmenu"=>"si",
            "validarjs" => array(),
            "permisos" => $permisos
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idbus = Input::get("id");
        $create = $this->index()->getData(); //->data();        
        //$create["configuraciongeneral"][2] = "editar";
        $create["objetos"][0]->ValorAnterior = $idbus;
        //dd($create);
        $param = [
            "checkmenu" => "si",
            "menutodo" => "si",
            "idbus" => $idbus
        ];
        $res = array_merge($create, $param);
        //dd($res);
        return view('permisosmenu.createindex', $res);
    }
    public function store(Request $request)
    {
        //
        return $this->guardar(0, $request);
    }
    public function guardar($id,$request)
        {            
            $input = $request->all();
            //show($input);

            $ruta=$this->configuraciongeneral[1];
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new PerfilPermisosModel;
                 $msg="Registro Creado Exitosamente...!";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= PerfilPermisosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
            }            
            $arrapas=array();
            
            $validator = Validator::make($input, PerfilPermisosModel::rules());
            
            if ($validator->fails()) {
                //die($ruta);
              if(!Input::has("idmenumain") && Input::has("name"))
              { 
                $del=PerfilPermisosModel::where('idperfil', '=', Input::get('perfil'))->delete(); 
                Session::flash('message', "Todos los permisos fueron eliminados para el perfil.");
                return redirect($this->configuraciongeneral[1]);
              }else{
                return redirect("$ruta")
                    ->withErrors($validator)
                    ->withInput();
              }
            }else {
                //die($ruta);
                        $sw=0;
            			$permimenu = Input::get('idmenumain');
                        $tipo=Input::get('perfil');
                        /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
                        //DB::table('menutipousuario')->where('idtipousuario', '=', $tipo)->delete();
                        $del=PerfilPermisosModel::where('idperfil', '=', $tipo)->delete();
                        //print_r($permimenu);
//                        if(is_array($permimenu))
  //                      {                        
                            foreach ($permimenu as $valor) {
                                echo "Menu:  $valor <br>";
                                $permisos = new PerfilPermisosModel;
                                $permisos->idperfil  = $tipo;
                                $permisos->idmenu     = $valor;
                                /*$verpa= DB::table('menutipousuario')->where(function($query)use($valor,$tipo){
                                    $query->where("idtipousuario",$tipo)->where("idmenu",$valor);*/
                                $verpa= PerfilPermisosModel::where(function($query)use($valor,$tipo){
                                    $query->where("idperfil",$tipo)->where("idmenu",$valor);

                                })->first();
                                if(!$verpa)
                                {
                                    $permisos->save();
                                }
                                $sw=1;
                                /*Ver padre del hijo*/
                                //$verpa= DB::table('menu')->where("id",$valor)->first();
                                $verpa=MenuModel::where("id",$valor)->first();
                                $idpa=intval($verpa->idmain);
                                $verpa= PerfilPermisosModel::where(function($query)use($idpa,$tipo){
                                    $query->where("idperfil",$tipo)->where("idmenu",$idpa);
                                })->first();
                                
                                if(!$verpa && $idpa!=0)
                                {
                                    $permisos = new PerfilPermisosModel;
                                    $permisos->idperfil  = $tipo;
                                    $permisos->idmenu     = $idpa;
                                    
                                    $permisos->save();
                                }
                                  
                            }
                            //Actualizar por permisos asignados a usuarios con este perfil
                            if($sw==1)
                            {
                                //Permisos por Perfil
                                $perfil= Input::get('perfil');
                                if($perfil)
                                {                                       
                                    AsignarPermisosModel::where('idusuario', function($query) use($perfil)
                                        {
                                            $query->select('id')
                                                ->from(with(new UsuariosModel)->getTable())
                                                ->where('id_perfil',$perfil);
                                        })->delete();
                                        $user= UsuariosModel::where('id_perfil',$perfil)->get();
                                        foreach ($user as $key => $value) {
                                            $permiper= PerfilPermisosModel::where("idperfil",$perfil)
                                                ->select(array('idmenu',DB::raw($value->id. " as idusuario")));
                                            $bindings = $permiper->getBindings();
                                            $insert = 'INSERT into ad_menuusuario (idmenu,idusuario) '
                                                    . $permiper->toSql();
                                            DB::insert($insert, $bindings);
                                        }
                                }
                                Auditoria("Asignación de Permisos a Perfil. ID Perfil $perfil");
                            }
                Auditoria("Asignación de Permisos por Perfil"." - ID: ". $tipo);   
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
             Session::flash('message', $msg);
           return redirect($this->configuraciongeneral[1]);
            }             
        }
}