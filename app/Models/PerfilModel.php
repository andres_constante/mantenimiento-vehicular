<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PerfilModel extends Model
{
    use HasFactory;
    protected $table = 'ad_perfil';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'perfil'=>'required|unique:ad_perfil'. ($id ? ",id,$id" : ''),
            ], $merge);
        } 
}
