<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsignarPermisosModel extends Model
{
    use HasFactory;
    protected $table = 'ad_menuusuario';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'name'=>'required',
			'idmenumain'=>'required'
		], $merge);
    } 
}
