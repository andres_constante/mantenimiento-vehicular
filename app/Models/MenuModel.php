<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AsignarPermisosModel;
use App\Models\PerfilPermisosModel;
class MenuModel extends Model
{
    use HasFactory;
    //ad_menu
    protected $table = 'ad_menu';
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'menu' => 'required|unique:menu' . ($id ? ",id,$id" : ''),
                'ruta' => 'required',
                'nivel' => 'required',
                'idmain' => 'required',
                'orden' => 'required|numeric'
            ],
            $merge
        );
    }
    public function getChildren($data, $line, $idusuario = 0,$tipobus="usuario")
    {
        $children = [];
        foreach ($data as $line1) {
            if ($line['id'] == $line1['parent']) {
                $ar = ['submenu' => $this->getChildren($data, $line1,$idusuario,$tipobus)];
                if ($idusuario == 0)
                    $ar['select'] = "NO";
                else {
                    if($tipobus=="usuario")
                        $selct = AsignarPermisosModel::where("idmenu", $line1["id"])
                            ->where("idusuario", $idusuario)->first();
                    else
                        $selct = PerfilPermisosModel::where("idmenu", $line1["id"])
                            ->where("idperfil", $idusuario)->first();
                    if ($selct)
                        $ar['select'] = "SI";
                    else
                        $ar['select'] = "NO";
                }
                $children = array_merge($children, [array_merge($line1, $ar)]);
            }
        }
        return $children;
    }

    public function optionsMenu($admin = "NO",$todo=0,$tipobus="usuario")
    {
        return $this->where(function ($query) use ($admin,$todo,$tipobus) {
            $query->where("visible", "SI");
            if ($admin == "SI")
                $query->where("adicional", "admin");
            else
                $query->where("adicional", "");
            if($todo!=0)
                if($tipobus=="usuario")
                    $query->whereIn("id",function($query2) use($todo){
                        $query2->select('idmenu')
                        ->from(with(new AsignarPermisosModel)->getTable())                
                        ->where('idusuario', $todo);
                    });
                else
                    $query->whereIn("id",function($query2) use($todo){
                        $query2->select('idmenu')
                        ->from(with(new PerfilPermisosModel)->getTable())                
                        ->where('idperfil', $todo);
                    });
        })
            ->select("*", "idmain as parent")
            ->orderBy("idmain", "asc")
            ->orderBy("orden", "asc")
            ->orderBy("id", "asc")
            ->get()
            ->toArray();
    }

    public static function menus($admin = "NO", $idusuario = 0,$todo=0,$tipobus="usuario")
    {
        $menus = new MenuModel();
        $data = $menus->optionsMenu($admin,$todo,$tipobus);
        //show($data);            
        $menuAll = [];
        $menusel = [];
        foreach ($data as $line) {
            $ar = ['submenu' => $menus->getChildren($data, $line,$idusuario,$tipobus)];
            if ($idusuario == 0)
                $ar['select'] = "NO";
            else {
                $selct = AsignarPermisosModel::where("idmenu", $line["id"])
                    ->where("idusuario", $idusuario)->first();
                if ($selct)
                    $ar['select'] = "SI";
                else
                    $ar['select'] = "NO";
            }
            $item = [array_merge($line, $ar)];
            $menuAll = array_merge($menuAll, $item);
        }
        return $menus->menuAll = $menuAll;
    }
}
