<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PerfilPermisosModel extends Model
{
    use HasFactory;
    protected $table = 'ad_perfilpermisos';
        public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'perfil'=>'required'/*,
                'idmenu'=>'required'*/
            ], $merge);
        } 
}
