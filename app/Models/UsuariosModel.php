<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsuariosModel extends Model
{
    use HasFactory;
    protected $table = 'users';
     protected $hidden = array('password', 'remember_token');
  public static function rulesedit($id = 0, $merge = [])
    {
        return array_merge(
            [                
                'name' => 'required',
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|unique:users' . ($id ? ",id,$id" : ''),
                'id_tipo_usuario' => 'required',
                'id_perfil'=> 'required'
                //'password' => 'required|alpha_num|min:4|required_with:password_confirm|same:password_confirm',
                //'password_confirm' => 'required|alpha_num|min:4'
            ],
            $merge
        );
    }
    public static function rulesnew($id = 0, $merge = [])
    {
        return array_merge([            
            'name' => 'required',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|unique:users' . ($id ? ",id,$id" : ''),
            'id_tipo_usuario' => 'required',
            'id_perfil'=> 'required'
            //'password' => 'required|alpha_num|min:4|required_with:password_confirm|same:password_confirm',
            //'password_confirm' => 'required|alpha_num|min:4'
        ], $merge);
    } 
}
