<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SysConfigModel extends Model
{
    use HasFactory;
    protected $table = 'ad_sysconfig';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'campo'=>'required|unique:ad_sysconfig'. ($id ? ",id,$id" : ''),
                'valor'=>'required'
            ], $merge);
        } 
}
