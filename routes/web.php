<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\TipousuarioController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\SysConfigController;
use App\Http\Controllers\AuditoriaController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\AsignarPermisosController;
use App\Http\Controllers\AsignarPermisosPerfilController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('inicio');
/*Usuarios*/
Route::resource("usuarios",UsuariosController::class);
Route::get('usuariosajax', [UsuariosController::class, 'usuariosajax']);
Route::get('/gettipousuario', [UsuariosController::class, 'gettipousuario'])->name("gettipousuario");
Route::get('/getperfil', [UsuariosController::class, 'getperfil'])->name("getperfil");

/*Tipos de usuario*/
Route::resource("tipousuario",TipousuarioController::class);
Route::get('tipousuarioajax', [TipousuarioController::class, 'tipousuarioajax']);
/*Tipos de usuario*/
Route::resource("perfil",PerfilController::class);
Route::get('perfilajax', [PerfilController::class, 'perfilajax']);
/*Variables de COnfiguración*/
Route::resource("config",SysConfigController::class);
Route::get('configajax', [SysConfigController::class, 'configajax']);
/*Auditoria*/
Route::resource("auditoria",AuditoriaController::class);
Route::get('auditoriaajax', [AuditoriaController::class, 'auditoriaajax']);
/*Menú de Opciones*/
Route::resource("menu",MenuController::class);
Route::get('menuajax', [MenuController::class, 'menuajax']);
/*Asignar Permisos*/
Route::resource("asignarpermisos",AsignarPermisosController::class);
Route::get("permisostipousu",[AsignarPermisosController::class, 'permisostipousu']);
Route::get("menutree",[AsignarPermisosController::class, 'menutree']);

//Route::get('asignarpermisos', [AsignarPermisosController::class, 'asignarpermisosajax']);
Route::resource("asignarpfilpermisos",AsignarPermisosPerfilController::class);

Route::get("llamarpermisoscrud",[AsignarPermisosController::class, 'llamarpermisoscrud']);
Route::post("llamarpermisoscrudajax",[AsignarPermisosController::class, 'llamarpermisoscrudajax']);